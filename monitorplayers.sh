#!/bin/bash

source comfunc.sh

while [ 1 -eq 1 ]; do
	for playerid in "$@"; do
		TSTAMP=$( date +'%D %T' )
		# get player data
		PLAYER_DATA=$( . apireq.sh user "$playerid" profile )

		# if no response then move on
		if [ -z "$PLAYER_DATA" ]
		then
			echo "$TSTAMP [${$playerid}] ERROR! Failed to retrieve a response."
			continue
		fi

		# process player info from json data
		PLAYER_NAME=$( echo "$PLAYER_DATA" | jq -r '.name' )
		PLAYER_ID=$( echo "$PLAYER_DATA" | jq -r '.player_id' )
		PLAYER_STATUS=$( echo "$PLAYER_DATA" | jq -r '([ .status[] | select(length > 0)] | join(", "))' | sed -e 's/<[^>]*>//g' )
		PLAYER_ACTLBL=$( echo "$PLAYER_DATA" | jq -r '.last_action.relative' )
		PLAYER_LASTEPOCH=$( echo "$PLAYER_DATA" | jq -r '.last_action.timestamp' )

		# calculate duration since last action
		NOW_EPOCH=$( date +%s )
		let "PLAYER_ACTDUR=($NOW_EPOCH-$PLAYER_LASTEPOCH)"
		PLAYER_LASTACT=$( durationlabel $PLAYER_ACTDUR )
		TSTAMP=$( date +'%D %T' )

		# calculate spacing for display
		IDPAD=$( makepadding "$PLAYER_ID" 7 " " )
		NAMEPAD=$( makepadding "$PLAYER_NAME" 15 "-" )
		ACTLBLPAD=$( makepadding "$PLAYER_ACTLBL" 14 " " )
		ACTSTPPAD=$( makepadding "$PLAYER_LASTACT" 10 "-" )

		# display info
		echo -e "$TSTAMP [${PLAYER_ID}] $IDPAD $PLAYER_NAME $NAMEPAD $PLAYER_ACTLBL $ACTLBLPAD (${PLAYER_LASTACT}) $ACTSTPPAD $PLAYER_STATUS"
	done
	sleep 30
done
