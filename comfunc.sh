#!/bin/bash

# function takes a number of seconds and returns a human readable duration string
durationlabel() {
        DUR_SECONDS=$1

	if [ -z "$DUR_SECONDS" ]
	then
		echo ""
	else
	        let "DUR_DAYS=$DUR_SECONDS/(60*60*24)"
        	let "DUR_SECONDS-=$DUR_DAYS*60*60*24"
	        let "DUR_HOURS=$DUR_SECONDS/(60*60)"
        	let "DUR_SECONDS-=$DUR_HOURS*60*60"
	        let "DUR_MINUTES=$DUR_SECONDS/60"
        	let "DUR_SECONDS-=$DUR_MINUTES*60"

	        echo "${DUR_DAYS}d${DUR_HOURS}h${DUR_MINUTES}m${DUR_SECONDS}s"
	fi
}

# repeats the specified character a number of times
repeatchar() {
	RPTCHAR=$1
	RPTLENGTH=$2

	RESULT=""
	ITER=0

	while [ $ITER -lt $RPTLENGTH ]
	do
		RESULT+="$RPTCHAR"
		let "ITER++"
	done

	echo "$RESULT"
}

# creates padding for a string
makepadding() {
	LBL=$1
	MAXLEN=$2
	PADCHAR=$3

	LBLLEN=${#LBL}
	let "LBLPAD_LEN=$MAXLEN-$LBLLEN"

	LBLPAD=$( repeatchar "$PADCHAR" "$LBLPAD_LEN" )
	echo "$LBLPAD"
}

# removes html
striphtml() {
	echo "$1" | sed -e 's/<[^>]*>//g'
}
