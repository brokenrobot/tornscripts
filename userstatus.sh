#!/bin/bash

./apireq.sh user "$1" basic | jq -r '([ .status[] | select(length > 0)] | join(", "))' | sed -e 's/<[^>]*>//g'
