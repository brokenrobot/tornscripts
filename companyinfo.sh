#!/bin/bash

COMP_NAME=$1

JSON_FILTER="select(.name==\"$COMP_NAME\")"

./apireq.sh torn "" companies | jq -r .companies[] | jq "$JSON_FILTER"
