#!/bin/bash
source comfunc.sh

PLAYER_DATA=$( . apireq.sh user "" bars,notifications,basic,timestamp )

NERVE_CUR=$( echo "$PLAYER_DATA" | jq '.nerve.current' )
NERVE_MAX=$( echo "$PLAYER_DATA" | jq '.nerve.maximum' )
NERVE_LBL="Nerve:${NERVE_CUR}/${NERVE_MAX}"

ENRGY_CUR=$( echo "$PLAYER_DATA" | jq '.energy.current' )
ENRGY_MAX=$( echo "$PLAYER_DATA" | jq '.energy.maximum' )
ENRGY_LBL="Energy:${ENRGY_CUR}/${ENRGY_MAX}"

HELTH_CUR=$( echo "$PLAYER_DATA" | jq '.life.current' )
HELTH_MAX=$( echo "$PLAYER_DATA" | jq '.life.maximum' )
HELTH_LBL="Life:${HELTH_CUR}/${HELTH_MAX}"

NOT_EVT=$( echo "$PLAYER_DATA" | jq '.notifications.events' )
NOT_MSG=$( echo "$PLAYER_DATA" | jq '.notifications.messages' )
NOT_LBL="Evt:${NOT_EVT} Msg:${NOT_MSG}"

USR_STAT=$( echo "$PLAYER_DATA" | jq -r '([ .status[] | select(length > 0)] | join(", "))' )
USR_STATC=$( striphtml "$USR_STAT" )

TSTMP_EPOCH=$( echo "$PLAYER_DATA" | jq '.timestamp' )
TSTMP=$( date -d "@${TSTMP_EPOCH}" +'%D %T' )


echo "$TSTMP $NERVE_LBL $ENRGY_LBL $HELTH_LBL $NOT_LBL $USR_STATC"
