#!/bin/bash

# This script reads your API key from a file named .apikey
# This file should probably be placed in the same directory as the scripts.
# Please secure this file as you don't want anyone else to ever know your API key.

if [ -f ".apikey" ]
then
	API_KEY=$( head -n 1 .apikey )

	API_FUNC="$1"

	API_PARAM="$2"

	API_SELEC="$3"

	API_URL="https://api.torn.com/$API_FUNC/$API_PARAM?selections=$API_SELEC&key=$API_KEY"

	curl -s "$API_URL"
else
	echo "The .apikey file was not found."
fi
